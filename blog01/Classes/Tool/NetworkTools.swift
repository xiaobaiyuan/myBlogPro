//
//  NetworkTools.swift
//  blog01
//
//  Created by 秦兴华 on 15/9/5.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit
import AFNetworking
import ReactiveCocoa

 //网络请求方式的枚举
enum RequestMethod : String {
    case GET = "GET"
    case POST = "POST"
}

class NetworkTools: AFHTTPSessionManager {

    private let clientId = "2135387363"
    private let appSecret = "0d9a62de851305baee6100651603eedf"
    
    //回调地址
    let redirectUri = "http://www.baidu.com"
    
    var oauthUrl: NSURL {
        let urlString = "https://api.weibo.com/oauth2/authorize?client_id=\(clientId)&redirect_uri=\(redirectUri)"
        
        return NSURL(string: urlString)!
    }
    
    /// 返回用户数据
    ///
    /// - parameter uid:用户数据
    /// - see[http://open.weibo.com/wiki/2/users/show](http://open.weibo.com/wiki/2/users/show)
    /// - returns: 返回用户部分数据
    func loadUserInfo(uid:String) ->RACSignal {
        //1.数据访问地址
        let url = "https://api.weibo.com/2/users/show.json"
        
        //2.参数
        let param = ["uid":uid]
        
//        return request(url, method: .GET, parameter: param, withToken:false)
        return request(url, method: .GET, parameter: param)
    }

    /// 获取accessToken
    ///
    /// - parameter code: 授权码
    ///
    /// - returns: 获取accessToken的返回数据
    /// - see: [http://open.weibo.com/wiki/OAuth2/access_token](http://open.weibo.com/wiki/OAuth2/access)
    func getAccseeToken(code:String) -> RACSignal{
        //1.拼接参数
       
        let params = ["client_id": clientId,
            "client_secret": appSecret,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": redirectUri]
        //2.请求地址
        let url = "https://api.weibo.com/oauth2/access_token"
        
        /************ 调试代码【获取二进制数据】begin ****************/
        //(0)设置返回值数据类型为二进制
//        responseSerializer = AFHTTPResponseSerializer()

        //(1)手动调用post截取二进制数据
//        POST(url, parameters: params, success: { (_, AnyObject) -> Void in
//            print(AnyObject)
//            let str = NSString(data: AnyObject as!NSData, encoding: NSUTF8StringEncoding)
//            print(str)
//            }) { (NSURLSessionDataTask, NSError) -> Void in
//                
//        }
//        return RACSignal.empty()
        /************ 调试代码【获取二进制数据】end ****************/
        
        
        //3.调用网络请求
 
        return request(url, method: .POST, parameter: params, withToken:false)
    }
   
    /// 创建一个网络工具单例
    static var sharedTools : NetworkTools = {
        //创建一个变量，设置基础URL
        var instance = NetworkTools(baseURL: nil)

        instance.responseSerializer.acceptableContentTypes?.insert("text/html")
        instance.responseSerializer.acceptableContentTypes?.insert("text/plain")
        
        return instance
    }()
    
    /// 网络请求方法
    ///
    /// - parameter Url:访问路径
    /// - parameter method:访问方式
    /// - parameter parameter: 范文参数
    /// - parameter withToken: 是否需要token，默认需要
     private func request(Url:String,method:RequestMethod, var parameter:[String:AnyObject]?,withToken:Bool = true ) ->RACSignal{
    
        return  RACSignal.createSignal ({ (subscriber) -> RACDisposable! in
            // 0. 判断是否需要 token
            if withToken {
                // 0.1判断 token 是否存在，guard 刚好是和 if let 相反
                guard let token = UserAccoutViewModel.sharedUserAccount.accessToken else{
                    subscriber.sendError(NSError(domain: "com.itheima.error", code: -1001, userInfo: ["errorMessage": "Token 为空"]))
                    return nil
                }
                // 需要增加`参数字典`中的token参数
                if parameter == nil{
                    parameter = [String:AnyObject]()
                }
                parameter!["access_token"] = token
            }
            
            //1.成功回调
            let successCallback = {
                (task:NSURLSessionDataTask, result:AnyObject) -> Void in
                //发送给订阅者
                subscriber.sendNext(result)
                //完成
                subscriber.sendCompleted()
            }
            //2.失败回调
            let failureCallback = {
                (task:NSURLSessionDataTask, error:NSError) -> Void in
                print(error)
                subscriber.sendError(error)
            }
            //根据访问方式访问网络
            if method == RequestMethod.GET{
                self.GET(Url, parameters: parameter, success: successCallback, failure:failureCallback)
            }else{
                self.POST(Url, parameters: parameter, success: successCallback, failure:failureCallback)
                
            }
            return nil
        })
    }
//     private func request(Url:String,method:RequestMethod, var parameter:[String:AnyObject]?,withToken:Bool = true ) ->RACSignal{
//        
//      return  RACSignal.createSignal ({ (subscriber) -> RACDisposable! in
//        // 0. 判断是否需要 token
//        // 需要增加`参数字典`中的token参数
//        // 0.1判断 token 是否存在，guard 刚好是和 if let 相反
//        if withToken{
//             guard let token = UserAccoutViewModel.sharedUserAccount.accessToken else{
//                subscriber.sendError(NSError(domain: "com.itheima.error", code: 1001, userInfo: ["error" : "Token 为空"]))
//                return nil
//            }
//            //0.2将token存储到参数字典
//            if parameter == nil{
//                //注意不要丢掉（）
//                parameter = [String:AnyObject]()
//            }
//            //后续token都是有值的
//            parameter!["access_token"] = token
//        }
//        
//        //1.成功回调
//        let successCallback = {
//                (task:NSURLSessionDataTask, result:AnyObject) -> Void in
//                //发送给订阅者
//                subscriber.sendNext(result)
//                //完成
//                subscriber.sendCompleted()
//            }
//        //2.失败回调
//        let failureCallback = {
//                (task:NSURLSessionDataTask, error:NSError) -> Void in
//                print(error)
//                subscriber.sendError(error)
//            }
//    
//        //根据访问方式访问网络
//        if method == RequestMethod.GET{
//            self.GET(Url, parameters: parameter, success: successCallback, failure:failureCallback)
//        }else{
//            self.POST(Url, parameters: parameter, success: successCallback, failure:failureCallback)
//        
//        }
//            return nil
//        })
//    }
    
    
}
