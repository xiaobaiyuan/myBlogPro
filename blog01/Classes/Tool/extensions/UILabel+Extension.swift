//
//  UILabel+Extension.swift
//  blog01
//
//  Created by 秦兴华 on 15/9/12.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit

extension UILabel {
    //便利构造函数
    convenience init(title:String, color:UIColor,textFont:CGFloat){
        //1、调用本类的构造函数
        self.init()
        
        //2、给对象赋值
        text = title
        textColor = color
        font = UIFont.systemFontOfSize(textFont)
    }
}

