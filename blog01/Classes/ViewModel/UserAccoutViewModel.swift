//
//  UserAccoutViewModel.swift
//  blog01
//
//  Created by 秦兴华 on 15/9/6.
//  Copyright © 2015年 heima. All rights reserved.
//
/**********  视图控制器的创建意义 *******************/
//1.view model 是一个放置用户输入验证逻辑，视图显示逻辑，`发起网络请求'和其他代码
//2.视图控制器（view/controller）不能与数据模型直接交互，
//3.token:每个用户只有唯一的一个token【单例】，外界访问token通过此视图模型，不在直接访问数据模型
//4.此视图模型做到【与数据模型进行交互，剥离网络访问】
// 获取用户信息和token

/************************************************/

import UIKit
import ReactiveCocoa

class UserAccoutViewModel: NSObject {
    //创建一个单例
    static let sharedUserAccount = UserAccoutViewModel()
    
    //初始化账户属性
    override init() {
        //从沙盒中获取数据
        userAccount = UserAccount.readAccountInfo()
    }
    
    //创建一个用户账户属性
    var userAccount : UserAccount?
    
    //创建一个用户token属性【只读】
    var accessToken:String?{
        return userAccount?.access_token
    }
    
    //用户登录标识
    var userLogon : Bool{
        return accessToken != nil
    }
    //MARK: - 加载用户信息
    func loadUserAccount(code:String) ->RACSignal {
    
        return RACSignal.createSignal { (subscriber) -> RACDisposable! in
          //网络方法获取token
            NetworkTools.sharedTools.getAccseeToken(code).doNext({ (result) -> Void in
                //字典转模型【用户账户】
                let account =  UserAccount(dict: result as![String:AnyObject])
                
                //给用户账户赋值
                self.userAccount = account

                print("account:\(account)")
                
//                subscriber.sendNext(account)
                //访问完成回调
//                subscriber.sendCompleted()
                
                NetworkTools.sharedTools.loadUserInfo(account.uid!).subscribeNext({ (result) -> Void in
                    //用字典接收返回数据
                    let dict = result as![String:AnyObject]
                    
                    account.name = dict["name"] as? String
                    account.avatar_large = dict["avatar_large"] as?String
                    
                    printLog(account)
                    
                    //存储账户数据
                    account.saveAccountInfo()
                    
                    //通知回调登录完成
                    subscriber.sendCompleted()
                    }, error: { (error) -> Void in
                        subscriber.sendError(error)
                    }, completed: {})
                
            }).subscribeError({ (error) -> Void in
                subscriber.sendError(error)
            })
            
            return nil
        }
    
    }
}
