//
//  userAccount.swift
//  blog01
//
//  Created by 秦兴华 on 15/9/6.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit

class UserAccount: NSObject,NSCoding {
    //MARK: - 声明属性
    //1.获取token时返回的数据
    var access_token: String?
    var expires_in:NSTimeInterval = 0{
        //{}里面定义的函数随着外部的变量变化而变化
        didSet{
            expiresDate = NSDate(timeIntervalSinceNow: expires_in)
        }
    }
    //下面属性将废弃，用上面的谁能够代替
    //var emind_in:String?
    
    var uid:String?
    //2.将过期时间进行格式化
    var expiresDate:NSDate?
    
    //3.获取部分用户信息/
    // 友好显示名称
    var name: String?
    /// 用户头像地址（大图），180×180像素
    var avatar_large: String?
    
    
    
    //kvc赋值，要重写构造含函数，注释的是错误写法
    init(dict:[String:AnyObject]) {
        super.init()
        //kvc
        setValuesForKeysWithDictionary(dict)
        
    }
//    注释的是错误写法
//    class func account(dict:[String:AnyObject]){
//        //kvc
//        setValuesForKeysWithDictionary(dict)
//    }

    override func setValue(value: AnyObject?, forUndefinedKey key: String) {}
    
    /// 重写输出函数description,打印对象格式
    override var description:String {
        //定义字典返回值对一个的key值
        let keys = ["access_token","expires_in","expiresDate","uid","name","avatar_large"];

        //description返回字典固有的打印格式
        return dictionaryWithValuesForKeys(keys).description
    }
    
    /// MARK: -文件的归档解档
    // 创建文件存储路径 :因为这个路径经常需要使用，且值规定，不被外部调用，故用static修饰，用'类型.'去调用
    //swift xcode 7.0 beta 5之后 不能直接在沙盒路径后面拼接文件路径，需要as 为NSString之后在进行操作
    static let path = (NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).last! as NSString).stringByAppendingPathComponent("account.plist")
    

    //存储文件
    func saveAccountInfo(){
        print(UserAccount.path)
        NSKeyedArchiver .archiveRootObject(self, toFile: UserAccount.path)
    }
    
    //解析文件（类方法）
    class func readAccountInfo()->UserAccount? {
        let account =  NSKeyedUnarchiver.unarchiveObjectWithFile(path) as?UserAccount
        
        if let date = account?.expiresDate{
            //过期时间和当前时间相比较，如果是降序（即过期时间>当前时间），标识账户未过期
            if date.compare(NSDate()) == .OrderedDescending{
                return account
            }
        }
        return nil
    }
    
    /// 归档
     func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(access_token, forKey: "access_token")
        aCoder.encodeObject(expiresDate, forKey: "expiresDate")
        aCoder.encodeObject(uid, forKey: "uid")
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(avatar_large, forKey: "avatar_large")
    }
    
    /// 解档【不需要像oc那样调用super init】
    required init?(coder aDecoder: NSCoder) {
        access_token = aDecoder.decodeObjectForKey("access_token") as? String
        expiresDate = aDecoder.decodeObjectForKey("expiresDate") as? NSDate
        uid = aDecoder.decodeObjectForKey("uid") as? String
        name = aDecoder.decodeObjectForKey("name") as? String
        avatar_large = aDecoder.decodeObjectForKey("avatar_large")as? String
    
    }
    
        
}
