//
//  OauthViewController.swift
//  blog01
//
//  Created by 秦兴华 on 15/9/3.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit

class OauthViewController: UIViewController,UIWebViewDelegate{

    //将Oauth的授权页面现在在UIWebView的组件上
    //定义一个webView
    private lazy var webView = UIWebView()
    
    private var userAccount : UserAccount?
    
    override func loadView() {
        view = webView
       
        //设置代理的时候报错，要看下有没有遵守协议
        webView.delegate = self
   
        title = "新浪微博登录"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "关闭", style: UIBarButtonItemStyle.Plain, target: self, action: "close")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "自动填充", style: UIBarButtonItemStyle.Plain, target: self, action: "autoFill")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //加载授权页面
        webView.loadRequest(NSURLRequest(URL: NetworkTools.sharedTools.oauthUrl))
        // print( NetworkTools.sharedTools.oauthUrl)
    }
    
    //关闭model页面
    @objc private func close(){
        
        dismissViewControllerAnimated(true, completion: nil)
    }

    //自动填充
    @objc private func autoFill(){
        
        let js = "document.getElementById('userId').value = '995043715@qq.com';" +
        "document.getElementById('passwd').value = 'qinxinghua@1989';"
        
        // 执行 js 脚本
        webView.stringByEvaluatingJavaScriptFromString(js)
    
    }
    
    /// 拦截用户跳转，获取授权码【使用webView的代理方法】
    // 通常在 iOS 开发中，如果代理方法有 Bool 类型的返回值，返回 true 通常是一切正常
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        //0.获取URL,并转换为字符串
        //**URL强行解包
        let requestUrl = request.URL!.absoluteString

        // 1. 判断 request.url 的前半部分是否是回调地址，如果不是回调地址，继续加载
        if !requestUrl.hasPrefix(NetworkTools.sharedTools.redirectUri){
        //    print("不是回调地址")
            return true
        }
        
        // 2. 如果是回调地址，检查 query，查询字符串，判断是否包含 "code="
        // query 就是 URL 中 `?` 后面的所有内容
        if let query = request.URL!.query where query.hasPrefix("code="){
        
        // 3. 如果有，获取 code
            let code  = query.substringFromIndex("code=".endIndex)
        
        // 4. 调用网络方法，获取 token
            UserAccoutViewModel.sharedUserAccount.loadUserAccount(code).subscribeNext({ (result) -> Void in
                print(result)
                }, error: { (error) -> Void in
                    print(error)
                }, completed: { () -> Void in
            })
        }else{
            print("取消")
        }
        
        return false
    }
}
