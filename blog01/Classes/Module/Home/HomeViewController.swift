//
//  HomeViewController.swift
//  blog01
//
//  Created by 秦兴华 on 15/8/31.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        visitorView?.setupVisitorView(nil, desc: "关注一些人，回这里看看有什么惊喜")
    }
}