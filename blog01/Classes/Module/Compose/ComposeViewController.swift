//
//  ComposeViewController.swift
//  blog01
//
//  Created by 秦兴华 on 15/9/10.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit

class ComposeViewController: UIViewController {
    
    //加载界面
    override func loadView() {
        //0.为控制器制定视图
        view = UIView()
        
        //1.设置视图背景色，避免出现晃一下
        view.backgroundColor = UIColor.whiteColor()
        
        //2.设置导航栏按钮
        setNav()
    }
    
    //设置导航栏
    private func setNav(){

        //1.添加发布和取消按钮
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "取消", style: UIBarButtonItemStyle.Plain, target: self, action: "close")
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "发送", style: UIBarButtonItemStyle.Plain, target: self, action: "composeBlog")
        
        //2.添加标题视图
        //2.1创建一个UIView
        let titleView = UIView(frame: CGRectMake(0, 0, 200, 32))
        
        //2.2为标题视图添加标题和用户名
        let titleLabel = UILabel(title: "发送微博", color: UIColor.darkGrayColor(), textFont: 15)
        let nameLabel = UILabel(title: UserAccoutViewModel.sharedUserAccount.userAccount!.name!, color: UIColor.darkGrayColor(), textFont: 13)
       
        //2.3将标题和用户名添加到标题视图
        titleView.addSubview(titleLabel)
        titleView.addSubview(nameLabel)
        
        //2.4设置标题和用户名布局
        titleLabel.ff_AlignInner(type: ff_AlignType.TopCenter, referView: titleView, size: nil)
        nameLabel.ff_AlignInner(type: ff_AlignType.BottomCenter, referView: titleView, size: nil)

        
        
        //2.5用户名label紧贴标题视图的底端
        navigationItem.titleView = titleView
        /* *************   手写的布局代码【未实现】  ************* */
        //        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        //        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        //        //2.3.1标题label紧贴标题视图的顶端
        //
        //        titleView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: titleView, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
        //
        //        titleView.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: titleView, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0))
    }
    
    //MARK: - 按钮监听方法
    ///关闭控制器
    @objc private func close(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    ///发布微博
    @objc private func composeBlog(){
    
    }
    
}
