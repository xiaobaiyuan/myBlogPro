//
//  ProfileViewController.swift
//  blog01
//
//  Created by 秦兴华 on 15/8/31.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        visitorView?.setupVisitorView("visitordiscover_image_profile", desc: "登录后，你的微博、相册、个人资料会显示在这里，展示给别人")

    }
}