//
//  DiscoverViewController.swift
//  blog01
//
//  Created by 秦兴华 on 15/8/31.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit

class DiscoverViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        visitorView?.setupVisitorView("visitordiscover_image_message", desc: "登录后，最新、最热微博尽在掌握，不再会与实事潮流擦肩而过")    }
}