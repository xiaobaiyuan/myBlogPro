//
//  MessageViewController.swift
//  blog01
//
//  Created by 秦兴华 on 15/8/31.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit

class MessageViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        visitorView?.setupVisitorView("visitordiscover_image_message",desc: "登录后，别人评论你的微博，发给你的消息，都会在这里收到通知")
    }

}