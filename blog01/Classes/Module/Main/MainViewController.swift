//
//  MainViewController.swift
//  blog01
//
//  Created by 秦兴华 on 15/8/31.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addChildController()
    }

    /// 调整加载顺序，让按钮晚于tabBarButton
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        //为了调试
//        print(tabBar.subviews)
        //调用此方法，会懒加载按钮
        setPlusBtn()
      
    }
   
    /// 1.自定义方法 添加子控制器
    private func addChildController(){
    
        //设置tabBar的渲染颜色
        tabBar.tintColor = UIColor.orangeColor()
        
        //首页
        addChildVc(HomeViewController(), title: "首页", img: "tabbar_home")
        
        //消息
        addChildVc(MessageViewController(), title: "消息", img: "tabbar_message_center")
        
        //中间的加号按钮
        
        let plus = UIViewController()
        
        addChildViewController(plus)
        
        //发现
        addChildVc(DiscoverViewController(), title: "发现", img: "tabbar_discover")

        //我
        addChildVc(ProfileViewController(), title: "我", img: "tabbar_profile")
}
    ///抽取方法,添加子控制器
    private func addChildVc(vc:UIViewController, title:String, img:String){
      
        
        vc.title = title
        
        vc.tabBarItem.image = UIImage(named:img)
        
        let nav = UINavigationController(rootViewController: vc)
        
        addChildViewController(nav)
        
    }
    
    //MARK: - 设置加号的按钮的位置
    private func setPlusBtn(){
        //获取tabBar的按钮数
        //let count = tabBar.subviews.count
        let count = childViewControllers.count
        
        //获取按钮宽度
        let width = tabBar.frame.width / CGFloat(count)
        
        //设置按钮的frame
        //CGRectOffset在给定的frame基础上进行偏移，此处是x 偏移两个宽度
        let rect = CGRectMake(0, 0, width, tabBar.frame.height)
        
        plusBtn.frame = CGRectOffset(rect, 2 * width, 0)
    
    }
  
    //MARK: - 懒加载中间加号button
    lazy var plusBtn : UIButton = {
        //创建button
        let btn = UIButton()
        
        //设置图片
        btn.setImage(UIImage(named: "tabbar_compose_icon_add"), forState: UIControlState.Normal)
        btn.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), forState: UIControlState.Highlighted)
        
        //设置背景
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: UIControlState.Normal)
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: UIControlState.Highlighted)
        
        //添加监听方法
        btn.addTarget(self, action: "plusBtnPress", forControlEvents: UIControlEvents.TouchUpInside)
        
        //将按钮添加到tabBar
        self.tabBar.addSubview(btn)
        return btn
        
    }()
    
    //MARK: - 按钮的监听方法
    // @objc 保证在私有方法的情况下 事件循环还能调用
    @objc private func plusBtnPress(){
        /// 定义一个常量来接收发布微博的控制器：
        //常量又一次赋值的机会，学习一下下面的写法
        //1.定义常量
        let vc:UIViewController
        
        //判断用户是否登录 ? 跳转发布微博界面 : 登录
        if UserAccoutViewModel.sharedUserAccount.userLogon == true {
            vc = ComposeViewController()
        }else{
            vc = BaseViewController()
        }
        
        let nvc = UINavigationController(rootViewController: vc)
        presentViewController(nvc, animated: true, completion: nil)
    }
}
