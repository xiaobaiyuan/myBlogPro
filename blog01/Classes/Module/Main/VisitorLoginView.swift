//
//  visitorLoginView.swift
//  blog01
//
//  Created by 秦兴华 on 15/9/2.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit
//protocol VisitorLoginViewDelegate:NSObjectProtocol
//{
//    func visitorLoginViewRegister();
//    func visitorLoginViewLogin();
//}


class VisitorLoginView: UIView {
   
//    weak var delegate:VisitorLoginViewDelegate?
//    
//    ///  注册的监听方法
//    @objc private func register(){
//   
//        delegate?.visitorLoginViewRegister()
//    }
//    ///  登录的监听方法
//    @objc private func login(){
//        
//        delegate?.visitorLoginViewLogin()
//    }
    
    /// 对外提供一个放着，设置访客视图
    func setupVisitorView(iconImage:String?, desc:String){
        //设置描述信息
        descView.text = desc
        if let imageName = iconImage{
            cicleView.image = UIImage(named: imageName)
            iconView.hidden = true
            sendSubviewToBack(maskIconView)
        }else{
            setAnimation()
        }
    }
    
    /// 首页动画
    private func setAnimation(){
        let anim = CABasicAnimation(keyPath: "transform.rotation")
        anim.toValue = 2 * M_PI
        anim.duration = 5
        anim.repeatCount = MAXFLOAT
        //设置动画不被删除，当cicleView销毁时动画也被删除
        anim.removedOnCompletion = false
        cicleView.layer.addAnimation(anim, forKey: "nil")
    }
    /// 纯代码开发会被调用
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    /// SB开发会被调用
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    
    //设置访客页面
    private func setupUI(){
        //添加子视图
        addSubview(cicleView)
        addSubview(maskIconView)
        addSubview(iconView)
        addSubview(descView)
        addSubview(registerView)
        addSubview(loginView)
        
        
        // 若设定控件支持自动布局，需将
        // translatesAutoresizingMaskIntoConstraints 设置为 false
        // 1.圆环的自动布局
        cicleView.translatesAutoresizingMaskIntoConstraints = false
        //1.1水平居中
        addConstraint(NSLayoutConstraint(item: cicleView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
        //1.2垂直居中
        addConstraint(NSLayoutConstraint(item: cicleView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0))
        
        // 2.房子的自动布局
        iconView.translatesAutoresizingMaskIntoConstraints = false
        //2.1水平居中
        addConstraint(NSLayoutConstraint(item: iconView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: cicleView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
        //2.2垂直居中
        addConstraint(NSLayoutConstraint(item: iconView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: cicleView, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0))
        
        //3.描述文字自动布局
        descView.translatesAutoresizingMaskIntoConstraints = false
        //3.1水平居中
        addConstraint(NSLayoutConstraint(item: descView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: cicleView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
        //3.2顶部据圆环底部16间距
        addConstraint(NSLayoutConstraint(item: descView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: cicleView, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 16))
        //3.2设置label的宽度
        //当设定一个固定的值，不是相对值，那么被参考的组件设置为nil attribute设置为 NSLayoutAttribute.NotAnAttribute
        // 提示：如果要设置一个固定数值，参照的属性，需要设置为 NSLayoutAttribute.NotAnAttribute，参照对象是 nil
        addConstraint(NSLayoutConstraint(item: descView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 224))
        
        //4.注册按钮自动布局
        registerView.translatesAutoresizingMaskIntoConstraints = false
        //4.1左侧与label对齐
        addConstraint(NSLayoutConstraint(item: registerView, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: descView, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: 0))
        //4.2顶部据label16间距
        addConstraint(NSLayoutConstraint(item: registerView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: descView, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 16))
        //4.3设定宽度
        addConstraint(NSLayoutConstraint(item: registerView, attribute: NSLayoutAttribute.Width , relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100))
        //4.4设定高度
        addConstraint(NSLayoutConstraint(item: registerView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 35))
        
        //5.注册按钮自动布局
        loginView.translatesAutoresizingMaskIntoConstraints = false
        //5.1右侧与label对齐
        addConstraint(NSLayoutConstraint(item: loginView, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: descView, attribute: NSLayoutAttribute.Right, multiplier: 1, constant: 0))
        //5.2顶部据label16间距
        addConstraint(NSLayoutConstraint(item: loginView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: descView, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 16))
        //5.3设定宽度
        addConstraint(NSLayoutConstraint(item: loginView, attribute: NSLayoutAttribute.Width , relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100))
        //5.4设定高度
        addConstraint(NSLayoutConstraint(item: loginView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 35))
        
        //6.遮罩
        maskIconView.translatesAutoresizingMaskIntoConstraints = false
        //6.1宽度是屏幕宽度
        addConstraint(NSLayoutConstraint(item: maskIconView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Width, multiplier: 1, constant: 0))
        //6.2
        addConstraint(NSLayoutConstraint(item: maskIconView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: maskIconView, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: registerView, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0))
        
        
//        //6.遮罩
        maskIconView.translatesAutoresizingMaskIntoConstraints = false
    
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[h]-0-|", options: [], metrics: nil, views: ["h":maskIconView]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[v]-(-35)-[regs]", options: [], metrics: nil, views: ["v":maskIconView,"regs":registerView]))
    //VFL的调试方法
//        let con = addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[v]-0-|", options: [], metrics: nil, views: ["v":maskIconView]))
//        print(con)
        
        
        //7.设置背景
        backgroundColor = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1)
        backgroundColor = UIColor(white: 237.0/255.0, alpha: 1.0)
       
        
    }
    //MARK:懒加载控件 --> 负责创建和设置控件的属性，但步对其位置等进行设置
    
    //圆环
    private lazy var cicleView : UIImageView  = UIImageView(image: UIImage(named:"visitordiscover_feed_image_smallicon"))

    //房子
    private lazy var iconView : UIImageView = UIImageView(image: UIImage(named:"visitordiscover_feed_image_house"))
    
    //描述
    private lazy var descView : UILabel = {
        let desc = UILabel()
        desc.text = "关注一些人，回这里看看有什么惊喜关注一些人，回这里看看有什么惊喜"
        desc.textColor = UIColor.lightGrayColor()
        desc.font = UIFont.systemFontOfSize(14)
        desc.numberOfLines = 0
        //自动布局已经设置好了其宽度，所以不用sizeToFit
        return desc
        }()
    
    //注册按钮【左】
    //private
    lazy var registerView : UIButton = {
        let register = UIButton()
        register .setTitle("注册", forState: UIControlState.Normal)
        register.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
        register.setTitleColor(UIColor.orangeColor(), forState: UIControlState.Normal)
//        register.addTarget(self, action:"register", forControlEvents: UIControlEvents.TouchUpInside)
        return register
        }()
    
    //登录按钮
    //private
    lazy var loginView : UIButton = {
        let login = UIButton()
        login .setTitle("登录", forState: UIControlState.Normal)
        login.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
        login.setTitleColor(UIColor.orangeColor(), forState: UIControlState.Normal)
//        login.addTarget(self, action: "login", forControlEvents: UIControlEvents.TouchUpInside)
        return login
        }()
    
    //遮罩
   private  lazy var maskIconView : UIImageView = UIImageView(image: UIImage(named:"visitordiscover_feed_mask_smallicon"))

    


}
