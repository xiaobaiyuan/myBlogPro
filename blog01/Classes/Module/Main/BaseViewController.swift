//
//  BaseViewController.swift
//  blog01
//
//  Created by 秦兴华 on 15/9/2.
//  Copyright © 2015年 heima. All rights reserved.
//

import UIKit

class BaseViewController: UITableViewController{

    
    //访客视图标识【通过token值来判断是否登录】
//    let userLogon = false
    let userLogon = UserAccoutViewModel.sharedUserAccount.userLogon

    //自定义根视图view
    var visitorView : VisitorLoginView?
    
    //执行的顺序比viewDidLoad更早，主要作用是创建根视图，
    //如果视图不存在，会反复调用 loadView
    //且其创建视图的优先级最高，能屏蔽xib 和 SB的创建视图操作
    override func loadView() {
        userLogon ?super.loadView()  : setVisitorView()
    
    }
    
    /// 访客视图设置
    private func setVisitorView(){
        //获取自定义视图
        visitorView = VisitorLoginView()
        
        //为按钮添加点击方法
        visitorView?.registerView.addTarget(self, action: "visitorLoginViewRegister", forControlEvents: UIControlEvents.TouchUpInside)
        
        visitorView?.loginView.addTarget(self, action: "visitorLoginViewLogin", forControlEvents: UIControlEvents.TouchUpInside)
        
        //将根视图设置为自定义视图
        view = visitorView
//        view.backgroundColor = UIColor.whiteColor()
        
//        visitorView?.delegate = self
    
        //设置导航按钮
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: UIBarButtonItemStyle.Plain, target:self , action: "visitorLoginViewRegister")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: UIBarButtonItemStyle.Plain, target:self , action: "visitorLoginViewLogin")
        
    }
    
    //MARK: - VisitorLoginViewDelegate
    func visitorLoginViewLogin() {
        let nav = UINavigationController(rootViewController: OauthViewController())
        
        presentViewController(nav, animated: true, completion: nil)
        
        
    }
    
    func visitorLoginViewRegister() {
        print("注册")
    }
    
   
    
}
